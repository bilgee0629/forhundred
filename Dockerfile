FROM golang:latest
RUN mkdir -p /go/src/bitbucket.org/bilgee0629/forhundred
ADD . /go/src/bitbucket.org/bilgee0629/forhundred

RUN set -ex \
    && curl https://glide.sh/get | sh

RUN set -ex \
    && cd /go/src/bitbucket.org/bilgee0629/forhundred \
    && glide update


RUN go install bitbucket.org/bilgee0629/forhundred
ENTRYPOINT /go/bin/forhundred

EXPOSE 80
