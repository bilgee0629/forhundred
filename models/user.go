package models

import (
	"encoding/json"
	"errors"
	"io"
	"regexp"
	"strings"

	"bitbucket.org/bilgee0629/forhundred/utils"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id            int    `json:"id"`
	Name          string `json:"name"`
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	Password      string `json:"password"`
	CreatedAt     int64  `json:"created_at"`
	UpdatedAt     int64  `json:"updated_at"`
}

func UserFromJson(data io.Reader) *User {
	decoder := json.NewDecoder(data)
	var user User
	err := decoder.Decode(&user)
	if err == nil {
		return &user
	}

	return nil
}

func (u *User) PreSave() {
	u.Email = strings.ToLower(u.Email)

	u.CreatedAt = utils.GetMillis()
	u.UpdatedAt = u.CreatedAt

	if len(u.Password) > 0 {
		u.Password = HashPassword(u.Password)
	}
}

func (u *User) PreUpdate() {
	//TODO!
}

func (u *User) IsValid() error {

	if u.Name == "" {
		return errors.New("Name must not be empty")
	}

	if u.CreatedAt == 0 {
		return errors.New("CreatedAt must be a valid time")
	}

	if u.UpdatedAt == 0 {
		return errors.New("UpdatedAt must be a valid time")
	}

	if !IsValidEmail(u.Email) {
		return errors.New("Invalid email")
	}

	if len(u.Password) == 0 {
		return errors.New("invalid password")
	}

	return nil
}

func IsValidEmail(email string) bool {

	re := regexp.MustCompile(`^([a-z0-9\.\-_]+@[a-z0-9_\-]+\.[a-z]+)$`)
	if re.MatchString(email) {
		return true
	}

	return false
}

func HashPassword(password string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		panic(err)
	}

	return string(hash)
}
