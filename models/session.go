package models

import "bitbucket.org/bilgee0629/forhundred/utils"

const (
	SESSION_CACHE_SIZE = 10000
)

type Session struct {
	Id             int    `json:"id"`
	CreatedAt      int64  `json:"created_at"`
	ExpiresAt      int64  `json:"expires_at"`
	LastActivityAt int64  `json:"last_activity_at"`
	Token          string `json:"token"`
	UserId         int    `json:"user_id"`
}

func (ss *Session) PreSave() {
	// if ss.Id == "" {
	// 	ss.Id = NewId()
	// }

	ss.Token = NewId()

	ss.CreatedAt = utils.GetMillis()
	ss.LastActivityAt = ss.CreatedAt

}

func (ss *Session) IsExpired() bool {
	if ss.ExpiresAt <= 0 {
		return false
	}

	if utils.GetMillis() > ss.ExpiresAt {
		return true
	}

	return false
}

func (ss *Session) SetExpireInDays(days int) {
	//86400000 = 1000 * 60 * 60 * 24
	ss.ExpiresAt = utils.GetMillis() + (86400000 * int64(days))
}
