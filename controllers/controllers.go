package controllers

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"

	"bitbucket.org/bilgee0629/forhundred/models"
	"bitbucket.org/bilgee0629/forhundred/utils"
)

var projectDir = os.Getenv("GOPATH") + "/src/bitbucket.org/bilgee0629/forhundred/public/"

var Templates = template.Must(template.ParseFiles(projectDir+"contact.html", projectDir+"order.html", projectDir+"login.html", projectDir+"dashboard.html", projectDir+"front.html", projectDir+"signup.html"))

var sessionCache *utils.Cache = utils.NewLru(models.SESSION_CACHE_SIZE)

func ContactHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		RenderTemplate(w, "contact", nil)
		return
	}
}

func FaqHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("faq handler is called")
	fmt.Fprintf(w, "%s", "Hello world from faq handler")
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		if IsAuthenticated(w, r) {
			http.Redirect(w, r, "/dashboard", http.StatusFound)
		}

		RenderTemplate(w, "login", nil)
	} else {
		email := r.PostFormValue("email")
		password := r.PostFormValue("password")

		if result := <-Srv.Store.User().GetByEmail(email); result.Err != nil {
			err := utils.Error{
				ErrorCode: 400,
				Message:   "Username or Password is wrong",
			}
			payload := Payload{
				Error: err,
			}
			RenderTemplate(w, "login", payload)

		} else if err := bcrypt.CompareHashAndPassword([]byte(result.Data.(models.User).Password), []byte(password)); err != nil {
			//when credentials are wrong
			err := utils.Error{
				ErrorCode: 400,
				Message:   "Username or Password is wrong",
			}
			payload := Payload{
				Error: err,
			}
			RenderTemplate(w, "login", payload)

		} else {

			fmt.Println("successfully logged in mothafucker")
			session := &models.Session{UserId: result.Data.(models.User).Id}

			if res := <-Srv.Store.Session().Save(session); res.Err != nil {
				fmt.Println("error while saving session", res.Err)
			} else {

				session = res.Data.(*models.Session)
				// fmt.Println("saved session", session)

				//session cache
				// sessionCache.AddWithExpiresInSecs(session.Token, session, int64(*utils.Cfg.ServiceSettings.SessionCacheInMinutes*60))
				Srv.CacheStore.SaveNewSession(session.Token, session.UserId)

				//do cookie operations
				cookie := &http.Cookie{
					Name:     "session_cookie",
					Value:    session.Token,
					Path:     "/",
					MaxAge:   1728000, //20 * 60 * 60 * 24
					Expires:  time.Unix(utils.GetMillis()/1000+int64(1728000), 0),
					HttpOnly: true,
				}
				http.SetCookie(w, cookie)

				http.Redirect(w, r, "/dashboard", http.StatusFound)
			}
		}
	}
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {

	oldCookie, err := r.Cookie("session_cookie")
	if err != nil {
		fmt.Println("error occured while getting cookie", err)
		return
	}

	if ok, err := Srv.CacheStore.DoesExists(oldCookie.Value); err != nil {
		fmt.Println("Error while checking session from cache", err)
	} else {
		if ok {
			if result := <-Srv.Store.Session().RemoveByToken(oldCookie.Value); result.Err != nil {
				fmt.Println("error while removing session from sql db")
			}
		} else {
			fmt.Println("session not found in cache")
		}
	}

	err = Srv.CacheStore.Remove(oldCookie.Value)
	if err != nil {
		fmt.Println("error occured while removing session from cache", err)
	}

	//logout operations
	cookie := &http.Cookie{
		Name:     "session_cookie",
		Value:    "",
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}

	http.SetCookie(w, cookie)

	http.Redirect(w, r, "/", http.StatusFound)
}

func SignupHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		if IsAuthenticated(w, r) {
			http.Redirect(w, r, "/dashboard", http.StatusFound)
		}
		RenderTemplate(w, "signup", nil)
	} else {
		name := r.PostFormValue("name")
		email := r.PostFormValue("email")
		password := r.PostFormValue("password")
		repeatPassword := r.PostFormValue("repeatPassword")

		//when two passwords don't match
		if password != repeatPassword {
			err := utils.Error{
				ErrorCode: 500,
				Message:   "Password doesn't match",
			}
			payload := Payload{
				Fields: map[string]string{
					"name":  name,
					"email": email,
				},
				Error: err,
			}
			RenderTemplate(w, "signup", payload)
		}

		//when password is invalid
		if len(password) > 40 || len(password) < 5 {
			err := utils.Error{
				ErrorCode: 500,
				Message:   "Password is invalid",
			}
			payload := Payload{
				Fields: map[string]string{
					"name":  name,
					"email": email,
				},
				Error: err,
			}
			RenderTemplate(w, "signup", payload)
		}

		u := models.User{
			Name:     name,
			Email:    email,
			Password: strings.TrimSpace(password),
		}

		if result := <-Srv.Store.User().Save(&u); result.Err != nil {
			fmt.Println("Error occured", result.Err)
			return
		} else {
			fmt.Println("SUCCESS!", result)
		}
	}
}

//after user signed in
func OrderHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		if !IsAuthenticated(w, r) {
			http.Redirect(w, r, "/login", http.StatusFound)
		}
		RenderTemplate(w, "order", nil)
		return
	}
}

func DashboardHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("dashboard handler is called")
	if r.Method == "GET" {
		if ok := IsAuthenticated(w, r); !ok {
			fmt.Println("fuck you")
			http.Redirect(w, r, "/login", http.StatusFound)
		}
		RenderTemplate(w, "dashboard", nil)
	}
}

func FrontHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("front handler is called")
	if r.Method == "GET" {
		RenderTemplate(w, "front", nil)
	}
}

type Payload struct {
	Fields interface{}
	Error  utils.Error
}

//template render helper function
func RenderTemplate(w http.ResponseWriter, fileName string, payload interface{}) {
	err := Templates.ExecuteTemplate(w, fileName+".html", payload)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
