package controllers

import (
	"fmt"
	"net/http"
	// "bitbucket.org/bilgee0629/forhundred/models"
)

func IsAuthenticated(w http.ResponseWriter, r *http.Request) bool {

	cookie, err := r.Cookie("session_cookie")
	if err != nil {
		fmt.Println("error while getting cookie", err)
		return false
	}

	token := cookie.Value

	if ok, err := Srv.CacheStore.DoesExists(token); err != nil {
		fmt.Println("error while checking cookie", err)
	} else if ok {
		return true
	} else {
		cookie := &http.Cookie{
			Name:     "session_cookie",
			Value:    "",
			Path:     "/",
			MaxAge:   -1,
			HttpOnly: true,
		}

		http.SetCookie(w, cookie)
	}

	// if session, ok := sessionCache.Get(value); ok {
	// 	fmt.Println("session from cache:", session.(*models.Session))
	// 	return true
	// }

	return false
}
