package controllers

import (
	"net/http"
	"os"

	"bitbucket.org/bilgee0629/forhundred/store"

	"time"

	"bitbucket.org/bilgee0629/forhundred/utils"
	l4g "github.com/alecthomas/log4go"
	"github.com/braintree/manners"
	"github.com/gorilla/mux"
)

type Server struct {
	Server     *manners.GracefulServer
	Store      store.Store
	CacheStore *store.RedisStore
	Router     *mux.Router
}

var Srv *Server

func NewServer() {
	l4g.Info("Server is initializing...")
	var httpServer http.Server
	// httpServer.Addr = ":" + os.Getenv("APP_PORT")
	httpServer.Addr = utils.Cfg.ServiceSettings.ListenAddress

	Srv = &Server{}

	Srv.Server = manners.NewWithServer(&httpServer)
	Srv.Router = mux.NewRouter()
	Srv.Store = store.NewSqlStore()
	Srv.CacheStore = store.NewRedisSessionStore()
}

func StartServer() {
	l4g.Info("Starting server...")
	l4g.Info("Server is listening at " + utils.Cfg.ServiceSettings.ListenAddress)

	Srv.Router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(os.Getenv("GOPATH")+"/src/bitbucket.org/bilgee0629/forhundred/public/"))))
	Srv.Router.HandleFunc("/", FrontHandler)
	Srv.Router.HandleFunc("/login", LoginHandler)
	Srv.Router.HandleFunc("/logout", LogoutHandler)
	Srv.Router.HandleFunc("/signup", SignupHandler)
	Srv.Router.HandleFunc("/contact", ContactHandler)
	Srv.Router.HandleFunc("/order", OrderHandler)
	Srv.Router.HandleFunc("/dashboard", DashboardHandler)

	var handler http.Handler = Srv.Router
	Srv.Server.Server.Handler = handler

	go func() {
		err := Srv.Server.ListenAndServe()
		if err != nil {
			l4g.Critical("Error starting the server, err :%v", err)
			time.Sleep(time.Second)
			panic("Error starting the server" + err.Error())
		}
	}()
}

func StopServer() {
	l4g.Info("Stopping the server...")
}
