package store

import (
	"errors"

	"bitbucket.org/bilgee0629/forhundred/models"
)

type SqlUserStore struct {
	*SqlStore
}

func NewSqlUserStore(sqlStore *SqlStore) UserStore {
	sql := &SqlUserStore{sqlStore}

	for _, db := range sqlStore.GetAllConns() {
		table := db.AddTableWithName(models.User{}, "users").SetKeys(true, "Id")
		table.ColMap("Id").SetMaxSize(26)
		table.ColMap("Name").SetMaxSize(64)
		table.ColMap("Password").SetMaxSize(128)
		table.ColMap("Email").SetMaxSize(128)
	}

	//do some tuning/tweaks here
	return sql
}

func (s SqlUserStore) Save(user *models.User) StoreChannel {
	storeChannel := make(StoreChannel)

	go func() {
		result := StoreResult{}

		user.PreSave()

		if result.Err = user.IsValid(); result.Err != nil {
			storeChannel <- result
			close(storeChannel)
			return
		}

		if oldUser := <-s.GetByEmail(user.Email); oldUser.Err == nil {
			result.Err = errors.New("user is already registered")
			storeChannel <- result
			close(storeChannel)
			return
		}

		if err := s.GetMaster().Insert(user); err != nil {
			result.Err = err
		} else {
			result.Data = user
		}

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (s SqlUserStore) Get(id string) StoreChannel {
	storeChannel := make(StoreChannel)

	go func() {
		result := StoreResult{}

		user := models.User{}

		if err := s.GetReplica().SelectOne(&user, "SELECT * FROM users WHERE id = :Id", map[string]interface{}{"Id": id}); err != nil {
			result.Err = err
		}

		result.Data = &user

		storeChannel <- result
		close(storeChannel)

	}()

	return storeChannel
}

func (s SqlUserStore) GetByEmail(email string) StoreChannel {
	storeChannel := make(StoreChannel)

	go func() {
		result := StoreResult{}

		user := models.User{}

		if err := s.GetReplica().SelectOne(&user, "SELECT * FROM users WHERE email = :Email", map[string]interface{}{"Email": email}); err != nil {
			result.Err = err
		}

		result.Data = user

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (s SqlUserStore) Update(user *models.User) StoreChannel {
	storeChannel := make(StoreChannel)

	go func() {
		result := StoreResult{}

		user.PreUpdate()

		if result.Err = user.IsValid(); result.Err != nil {
			storeChannel <- result
			close(storeChannel)
			return
		}

		if oldUserResult, err := s.GetMaster().Get(models.User{}, user.Id); err != nil {
			result.Err = err
		} else if oldUserResult == nil {
			result.Err = err
		} else {
			oldUser := oldUserResult.(*models.User)
			user.CreatedAt = oldUser.CreatedAt
			user.Password = oldUser.Password
			user.Email = oldUser.Email
			user.EmailVerified = oldUser.EmailVerified
			// user.FailedAttempts = oldUser.FailedAttempts

			if count, err := s.GetMaster().Update(user); err != nil {
				//!TODO IsUniqueConstraintError
				// result.Err = utils.NewLocAppError("SqlUserStore.Update", "store.sql_user.update", nil, "username="+user.Username+" "+err.Error())
				result.Err = err
			} else if count != 1 {
				// result.Err = utils.NewLocAppError("SqlUserStore.Update", "store.sql_user.update.app_error", nil, fmt.Sprintf("user_id=%v, count=%v", user.Id, count))
				result.Err = errors.New("too much failed attempts")
			} else {
				result.Data = [2]*models.User{user, oldUser}
			}
		}

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (s SqlUserStore) Remove(id string) StoreChannel {
	storeChannel := make(StoreChannel)

	go func() {
		result := StoreResult{}

		_, err := s.GetMaster().Exec("delete from users where id=?", id)
		if err != nil {
			result.Err = err
		}

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (s SqlUserStore) VerifyEmail(id int) StoreChannel {
	storeChannel := make(StoreChannel)

	go func() {
		result := StoreResult{}

		user := models.User{Id: id}

		if oldUserResult, err := s.GetMaster().Get(models.User{}, user.Id); err != nil {
			result.Err = err
		} else if oldUserResult == nil {
			result.Err = err
		} else {
			oldUser := oldUserResult.(*models.User)
			user.EmailVerified = true

			if count, err := s.GetMaster().Update(user); err != nil {
				//!TODO IsUniqueConstraintError
				// result.Err = utils.NewLocAppError("SqlUserStore.Update", "store.sql_user.update", nil, "username="+user.Username+" "+err.Error())
				result.Err = err
			} else if count != 1 {
				// result.Err = utils.NewLocAppError("SqlUserStore.Update", "store.sql_user.update.app_error", nil, fmt.Sprintf("user_id=%v, count=%v", user.Id, count))
				result.Err = errors.New("too much failed attempts")
			} else {
				result.Data = [2]*models.User{&user, oldUser}
			}
		}

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}
