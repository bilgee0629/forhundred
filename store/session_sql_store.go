package store

import (
	"fmt"

	"bitbucket.org/bilgee0629/forhundred/utils"

	"errors"

	"bitbucket.org/bilgee0629/forhundred/models"
)

type SqlSessionStore struct {
	*SqlStore
}

func NewSessionStore(sqlStore *SqlStore) SessionStore {
	sql := &SqlSessionStore{sqlStore}

	for _, db := range sqlStore.GetAllConns() {
		table := db.AddTableWithName(models.Session{}, "sessions").SetKeys(true, "Id")
		table.ColMap("Id").SetMaxSize(26)
		table.ColMap("Token").SetMaxSize(26)
		table.ColMap("UserId").SetMaxSize(26)
	}

	return sql
}

func (s SqlSessionStore) Save(session *models.Session) StoreChannel {
	storeChannel := make(StoreChannel)
	go func() {
		result := StoreResult{}

		if session.Id > 0 {
			result.Err = errors.New("session should be new")
			storeChannel <- result
			close(storeChannel)
			return
		}

		session.PreSave()

		if cur := <-s.CleanUpExpiredSessions(session.UserId); cur.Err != nil {
			fmt.Println("error mothafucker from CleanUpExpiredSessions", cur.Err)
		}

		if err := s.GetMaster().Insert(session); err != nil {
			result.Err = err
		}

		result.Data = session

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (s SqlSessionStore) CleanUpExpiredSessions(userId int) StoreChannel {
	storeChannel := make(StoreChannel)

	go func() {
		result := StoreResult{}

		if _, err := s.GetMaster().Exec(fmt.Sprintf("DELETE FROM sessions WHERE userid=%v AND expiresat != 0 AND %v > expiresat", userId, utils.GetMillis())); err != nil {
			result.Err = err
		} else {
			result.Data = userId
		}

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (s SqlSessionStore) Remove(id int) StoreChannel {
	storeChannel := make(StoreChannel)

	go func() {
		result := StoreResult{}

		if _, err := s.GetMaster().Exec(fmt.Sprintf("DELETE FROM sessions WHERE id=%v", id)); err != nil {
			result.Err = err
		}

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}

func (s SqlSessionStore) RemoveByToken(token string) StoreChannel {
	storeChannel := make(StoreChannel)

	go func() {
		result := StoreResult{}

		if _, err := s.GetMaster().Exec(fmt.Sprintf("DELETE FROM sessions WHERE token='%v'", token)); err != nil {
			result.Err = err
		}

		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}
