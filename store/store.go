package store

import (
	"bitbucket.org/bilgee0629/forhundred/models"
)

type Store interface {
	User() UserStore
	Session() SessionStore
}

type StoreResult struct {
	Data interface{}
	Err  error
}

type StoreChannel chan StoreResult

type UserStore interface {
	Save(user *models.User) StoreChannel
	Update(user *models.User) StoreChannel
	Get(id string) StoreChannel
	GetByEmail(email string) StoreChannel
	Remove(id string) StoreChannel
	VerifyEmail(id int) StoreChannel
}

type SessionStore interface {
	Save(session *models.Session) StoreChannel
	Remove(id int) StoreChannel
	RemoveByToken(token string) StoreChannel
}
