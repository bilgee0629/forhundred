package store

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/bilgee0629/forhundred/models"
	"bitbucket.org/bilgee0629/forhundred/utils"

	"math/rand"

	dbsql "database/sql"

	l4g "github.com/alecthomas/log4go"
	_ "github.com/lib/pq"
	"gopkg.in/gorp.v1"
)

type SqlStore struct {
	master   *gorp.DbMap
	replicas []*gorp.DbMap
	dbName   string
	user     UserStore
	session  SessionStore
}

func NewSqlStore() Store {
	fmt.Println("NewSqlStore is called")
	sqlStore := &SqlStore{}

	sqlStore.master = setupConnection("master", utils.Cfg.SqlSettings.DriverName,
		utils.Cfg.SqlSettings.DataSource, utils.Cfg.SqlSettings.MaxIdleConns,
		utils.Cfg.SqlSettings.MaxOpenConns, utils.Cfg.SqlSettings.Trace)

	if len(utils.Cfg.SqlSettings.DataSourceReplicas) == 0 {
		sqlStore.replicas = make([]*gorp.DbMap, 1)
		sqlStore.replicas[0] = setupConnection(fmt.Sprintf("replica-%v", 0), utils.Cfg.SqlSettings.DriverName,
			utils.Cfg.SqlSettings.DataSource, utils.Cfg.SqlSettings.MaxIdleConns,
			utils.Cfg.SqlSettings.MaxOpenConns, utils.Cfg.SqlSettings.Trace)
	} else {
		sqlStore.replicas = make([]*gorp.DbMap, len(utils.Cfg.SqlSettings.DataSourceReplicas))
		for i, replica := range utils.Cfg.SqlSettings.DataSourceReplicas {
			sqlStore.replicas[i] = setupConnection(fmt.Sprintf("replica-%v", i), utils.Cfg.SqlSettings.DriverName,
				replica, utils.Cfg.SqlSettings.MaxIdleConns,
				utils.Cfg.SqlSettings.MaxOpenConns, utils.Cfg.SqlSettings.Trace)
		}
	}

	sqlStore.dbName = utils.Cfg.SqlSettings.DatabaseName
	sqlStore.user = NewSqlUserStore(sqlStore)
	sqlStore.session = NewSessionStore(sqlStore)

	err := sqlStore.master.CreateTablesIfNotExists()
	if err != nil {
		l4g.Critical("store.sql.create_table_if_not_exits", err)
	}

	return sqlStore
}

func setupConnection(connType string, driver string, dataSource string, maxIdle int, maxOpen int, trace bool) *gorp.DbMap {
	db, err := dbsql.Open(driver, dataSource)
	if err != nil {
		l4g.Critical("store.sql.open_conn.critical", err)
		time.Sleep(time.Second)
		panic(fmt.Sprintf("store.sql.open_conn.panic - %v", err.Error()))
	}

	l4g.Info("store.sql.ping.critical", connType)
	err = db.Ping()
	if err != nil {
		l4g.Critical("store.sql.ping.critical", err)
		time.Sleep(time.Second)
		panic(fmt.Sprintf("store.sql.open_conn.panic - %v", err.Error()))
	}

	db.SetMaxIdleConns(maxIdle)
	db.SetMaxOpenConns(maxOpen)

	var dbmap *gorp.DbMap

	if driver == models.DATABASE_DRIVER_MYSQL {
		dbmap = &gorp.DbMap{Db: db, TypeConverter: forhundredConverter{}, Dialect: gorp.MySQLDialect{Engine: "InnoDB", Encoding: "UTF8MB4"}}
	} else if driver == models.DATABASE_DRIVER_POSTGRES {
		dbmap = &gorp.DbMap{Db: db, TypeConverter: forhundredConverter{}, Dialect: gorp.PostgresDialect{}}
	} else {
		l4g.Critical("store.sql.dialect_driver.critical")
		time.Sleep(time.Second)
		panic(fmt.Sprintf("store.sql.dialect_driver.panic: %v", err.Error()))
	}

	return dbmap
}

func (ss SqlStore) GetMaster() *gorp.DbMap {
	return ss.master
}

//returns random replica
func (ss SqlStore) GetReplica() *gorp.DbMap {
	return ss.replicas[rand.Intn(len(ss.replicas))]
}

func (ss SqlStore) GetAllConns() []*gorp.DbMap {
	all := make([]*gorp.DbMap, len(ss.replicas)+1)
	copy(all, ss.replicas)
	all[len(ss.replicas)] = ss.master
	return all
}

func (ss SqlStore) Close() {
	l4g.Info("Closing sql store")
	ss.master.Db.Close()
	for _, replica := range ss.replicas {
		replica.Db.Close()
	}
}

func (ss SqlStore) User() UserStore {
	return ss.user
}

func (ss SqlStore) Session() SessionStore {
	return ss.session
}

// type reportjoyConverter struct{}

type forhundredConverter struct{}

func (conv forhundredConverter) ToDb(val interface{}) (interface{}, error) {

	switch t := val.(type) {
	// case model.StringArray:
	case []string:
		return models.ArrayToJson(t), nil
	case models.StringMap:
		return models.MapToJson(t), nil
	}

	return val, nil
}

func (conv forhundredConverter) FromDb(target interface{}) (gorp.CustomScanner, bool) {
	fmt.Println("target", target)
	return gorp.CustomScanner{}, false
}

func IsUniqueConstraintError(err string, mysql string, postgres string) bool {
	unique := strings.Contains(err, "unique constraint") || strings.Contains(err, "Duplicate entry")
	field := strings.Contains(err, mysql) || strings.Contains(err, postgres)
	return unique && field
}
