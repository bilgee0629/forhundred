package store

import (
	"bitbucket.org/bilgee0629/forhundred/utils"
	"fmt"
	"github.com/garyburd/redigo/redis"
)

type RedisStore struct {
	pool *redis.Pool
}

func NewRedisSessionStore() *RedisStore {
	return &RedisStore{
		pool: &redis.Pool{
			MaxIdle:   100,
			MaxActive: 100000,
			Dial: func() (redis.Conn, error) {
				c, err := redis.Dial("tcp", fmt.Sprintf(":%v", utils.Cfg.CacheSettings.Port))
				if err != nil {
					panic(err)
				}
				return c, err
			},
		},
	}
}

func (store *RedisStore) SaveNewSession(token string, userId int) error {
	// _, err := store.pool.Get().Do("SET", token, userId)
	// if err != nil {
	// 	return err
	// }

	client := store.pool.Get()

	err := client.Send("SET", token, userId)
	if err != nil {
		return err
	}
	err = client.Send("EXPIRE", token, int64(*utils.Cfg.ServiceSettings.SessionCacheInMinutes*60))
	if err != nil {
		return err
	}

	_, err = client.Do("EXEC")
	if err != nil {
		return err
	}

	return nil
}

func (store *RedisStore) DoesExists(token string) (bool, error) {
	reply, err := store.pool.Get().Do("EXISTS", token)
	if err != nil {
		return false, err
	}
	if reply.(int64) == 1 {
		return true, nil
	}

	return false, nil
}

func (store *RedisStore) Remove(token string) error {
	_, err := store.pool.Get().Do("DEL", token)
	if err != nil {
		return err
	}

	return nil
}
