package main

import (
	"flag"
	"fmt"
	"syscall"
	// "forhundred/controllers"
	"bitbucket.org/bilgee0629/forhundred/controllers"
	"bitbucket.org/bilgee0629/forhundred/utils"
	// "net/http"
	"os"
	"os/signal"
)

var flagDev bool

func main() {
	flag.BoolVar(&flagDev, "dev", false, "")
	flag.Parse()

	fmt.Println("lol", os.Getenv("PGCLIENT_MASTER_USER"))

	if flagDev {
		utils.LoadConfig(os.Getenv("GOPATH") + "/src/bitbucket.org/bilgee0629/forhundred/config/config.json")
		fmt.Println("DEV!!!!!!!!!!!!!!")
	} else {
		utils.LoadConfig(os.Getenv("GOPATH") + "/src/bitbucket.org/bilgee0629/forhundred/config/prod_config.json")
	}

	controllers.NewServer()
	fmt.Println("new server worked great")
	controllers.StartServer()
	fmt.Println("start server worked great")

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-c

	controllers.StopServer()

	// http.Handle("/", http.StripPrefix("/", http.FileServer(http.Dir("./public"))))
	// http.HandleFunc("/contactus", controllers.ContactHandler)
	// http.HandleFunc("/order", controllers.OrderHandler)
	// http.HandleFunc("/faq", controllers.FaqHandler)
	// http.HandleFunc("/login", controllers.LoginHandler)
	// http.HandleFunc("/dashboard", controllers.DashboardHandler)

	// bind := fmt.Sprintf("%s:%s", os.Getenv("OPENSHIFT_GO_IP"), os.Getenv("OPENSHIFT_GO_PORT"))
	// fmt.Printf("listening on %s...", bind)
	// err := http.ListenAndServe(bind, nil)
	// if err != nil {
	// 	panic(err)
	// }
}

// func printer() string {
// 	return "Forhundred"
// }

// func hello(res http.ResponseWriter, req *http.Request) {
// 	fmt.Fprintf(res, "hello, world from %s. Welcome to %s", runtime.Version(), printer())
// }
