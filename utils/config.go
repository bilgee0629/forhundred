package utils

import (
	// l4g "reportjoy/Godeps/_workspace/src/github.com/alecthomas/log4go"
	"encoding/json"
	"fmt"
	"os"

	l4g "github.com/alecthomas/log4go"
)

const (
	MODE_DEV  = "dev"
	MODE_PROD = "prod"
)

var Cfg *Config = &Config{}
var CfgLastModified int64 = 0

type Config struct {
	ServiceSettings ServiceSettings
	LogSettings     LogSettings
	SqlSettings     SqlSettings
	EmailSettings   EmailSettings
	CacheSettings   CacheSettings
}

type ServiceSettings struct {
	ListenAddress          string
	MaximumLoginAttempts   int
	SessionCacheInMinutes  *int
	SessionLengthWebInDays *int
}

type LogSettings struct {
	EnableConsole bool
	EnableFile    bool
	FileLocation  string
}

type SqlSettings struct {
	DatabaseUsername   string
	DatabasePassword   string
	DriverName         string
	MaxIdleConns       int // ????????
	MaxOpenConns       int // ????????
	ServerName         string
	DatabaseName       string
	DataSource         string
	DataSourceReplicas []string
	Trace              bool
}

type EmailSettings struct {
	EnableSignUpWithEmail    bool
	EnableSignInWithEmail    bool
	EnableSignInWithUsername bool
	RequireEmailVerification bool
}

type CacheSettings struct {
	Port int
}

//filePath is an absolute path of the config file
func LoadConfig(filePath string) {
	file, err := os.Open(filePath)
	if err != nil {
		panic("Error opening config file " + filePath + "error: " + err.Error())
	}
	decoder := json.NewDecoder(file)
	config := Config{}
	err = decoder.Decode(&config)
	if err != nil {
		panic("Error decoding config file " + filePath + "error: " + err.Error())
	}
	l4g.Info("Successfully loaded configs")

	// config.SqlSettings.DataSource = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
	// config.SqlSettings.DataSource = fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable",
	// 	os.Getenv("FORHUNDRED_POSTGRES_1_PORT_5432_TCP_PORT"),
	// 	os.Getenv("POSTGRES_1_ENV_POSTGRES_USERNAME"),
	// 	os.Getenv("FORHUNDRED_POSTGRES_1_ENV_POSTGRES_DATABASE"),
	// )

	config.SqlSettings.DataSource = fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("FORHUNDRED_POSTGRES_USER"),
		os.Getenv("FORHUNDRED_POSTGRES_PASSWORD"),
		os.Getenv("FORHUNDRED_POSTGRES_DBNAME"),
	)

	fmt.Println("DATASOURCE DEBUG", config.SqlSettings.DataSource)

	Cfg = &config
}
