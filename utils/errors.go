package utils

type Error struct {
	ErrorCode int
	Message   string
}
