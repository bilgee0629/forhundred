build:
	go build -o forhundred .

config:
	export OPENSHIFT_GO_IP=localhost
	export OPENSHIFT_GO_PORT=8080

clean:
	rm forhundred

run:
	$(build)
	./forhundred


.PHONY: build config clean
